

<div class="md:flex flex-col 
            md:flex-row md:min-h-screen w-full ">

    <div @click.away="open = false" 
        class="flex flex-col w-full md:w-64 text-gray-300 
                dark-mode:text-gray-200 
                dark-mode:bg-gray-800 
                flex-shrink-0" 

        x-data="{ open: false }">
      
        <div class="flex-shrink-0 px-8 py-4 flex flex-row side_menu.items-center justify-between">

            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            Main
            </a>

            <button class="rounded-lg md:hidden  
                        focus:outline-none focus:shadow-outline " 

                @click="open = !open">
            
                <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
                <path x-show="!open" fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z" clip-rule="evenodd"></path>

                <path x-show="open" fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>

            
            </button>

        </div>

    <!-- My Navigation Side Menu Items -->
    <nav :class="{'block': open, 'hidden': !open}" class="flex-grow md:block px-4 pb-4 md:pb-0 md:overflow-y-auto">

    <!-- <x-side_menu.item> </x-side_menu.item> -->
        <x-side_menu.dropdown name="Dashboard" image="/images/Dashboard.png">
            <x-side_menu.item> <a href="/test">Admin Dashboard</a></x-side_menu.item>
            <x-side_menu.item>Employee Dashboard </x-side_menu.item>
        </x-side_menu.dropdown>

        <x-side_menu.dropdown name="Apps" image="/images/app.png">
            <x-side_menu.item>Chat </x-side_menu.item>
            <x-side_menu.item>Calls </x-side_menu.item>
            <x-side_menu.item>Calendar </x-side_menu.item>
            <x-side_menu.item>Contacts </x-side_menu.item>
            <x-side_menu.item>Email </x-side_menu.item>
            <x-side_menu.item>File Manager </x-side_menu.item>
        </x-side_menu.dropdown>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-100 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
                                Employees
            </a>
        </div>
        <x-side_menu.dropdown name="Employees">
            <a href="/">
                <x-side_menu.item>All Employees </x-side_menu.item>
            </a>
            <x-side_menu.item>Holidays </x-side_menu.item>
            <x-side_menu.item>Leaves (Admin) </x-side_menu.item>
            <x-side_menu.item> Leaves (Employee) </x-side_menu.item>
            <x-side_menu.item>Leave Settings </x-side_menu.item>
            <x-side_menu.item>Attendance (Admin) </x-side_menu.item>
            <x-side_menu.item>Attendance (Employee) </x-side_menu.item>
            <x-side_menu.item>Departments </x-side_menu.item>
            <x-side_menu.item>Designations </x-side_menu.item>
            <x-side_menu.item>Timesheet </x-side_menu.item>
            <x-side_menu.item>Shift & Schedule </x-side_menu.item>
            <x-side_menu.item>Overtime </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.title-item>Clients </x-side_menu.title-item>
        <x-side_menu.dropdown name="Project">
            <x-side_menu.item>Projects </x-side_menu.item>
            <x-side_menu.item>Tasks </x-side_menu.item>
            <x-side_menu.item>Task Board</x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.title-item>Leads </x-side_menu.title-item>
        <x-side_menu.title-item>Tickcets </x-side_menu.title-item>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            HR
            </a>
        </div>

        <x-side_menu.dropdown name="Sales" >
            <x-side_menu.item>Estimates </x-side_menu.item>                          
            <x-side_menu.item>Invoices </x-side_menu.item>        
            <x-side_menu.item>Payments </x-side_menu.item>            
            <x-side_menu.item>Expenses </x-side_menu.item>            
            <x-side_menu.item>Provident Fund </x-side_menu.item>            
            <x-side_menu.item>Taxes </x-side_menu.item>
        </x-side_menu.dropdown>

        <x-side_menu.dropdown name="Accounting" >
            <x-side_menu.item>Categories </x-side_menu.item>                          
            <x-side_menu.item>Budgets </x-side_menu.item>        
            <x-side_menu.item>Budget Expenses </x-side_menu.item>            
            <x-side_menu.item>Budget Revenues </x-side_menu.item> 
        </x-side_menu.dropdown>

        <x-side_menu.dropdown name="Payroll" >
            <x-side_menu.item>Employee Salary </x-side_menu.item>                          
            <x-side_menu.item>Payslip </x-side_menu.item>        
            <x-side_menu.item>Payroll side_menu.Items </x-side_menu.item>   
        </x-side_menu.dropdown>
        <x-side_menu.title-item>Policies </x-side_menu.title-item>
        
        <x-side_menu.dropdown name="Reports" >
            <x-side_menu.item>Expense Report </x-side_menu.item>                          
            <x-side_menu.item>Invoice Report </x-side_menu.item>        
            <x-side_menu.item>Payments Report </x-side_menu.item>   
            <x-side_menu.item>Project Report </x-side_menu.item>                          
            <x-side_menu.item>Task Report </x-side_menu.item>        
            <x-side_menu.item>User Report </x-side_menu.item>   
            <x-side_menu.item>Employee Report </x-side_menu.item>                          
            <x-side_menu.item>Payslip Report </x-side_menu.item>        
            <x-side_menu.item>Attendance Report </x-side_menu.item>   
            <x-side_menu.item>Leave Report </x-side_menu.item>                          
            <x-side_menu.item>Daily Report </x-side_menu.item>                    
        </x-side_menu.dropdown>
          

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            Performance
            </a>
        </div>

        <x-side_menu.dropdown name="Performance" >
            <x-side_menu.item>Performance Indicator </x-side_menu.item>
            <x-side_menu.item>Performance Review </x-side_menu.item>
            <x-side_menu.item>Performance Appraisal </x-side_menu.item>                 
        </x-side_menu.dropdown>

        <x-side_menu.dropdown name="Goals" >
            <x-side_menu.item>Goal List </x-side_menu.item>
            <x-side_menu.item>Goal Type </x-side_menu.item>               
        </x-side_menu.dropdown>

        <x-side_menu.dropdown name="Training " >
            <x-side_menu.item>Training List </x-side_menu.item>
            <x-side_menu.item>Trainers </x-side_menu.item>   
            <x-side_menu.item>Training Type </x-side_menu.item>               
        </x-side_menu.dropdown>
        <x-side_menu.title-item>Promotion </x-side_menu.title-item>
        <x-side_menu.title-item>Resignation </x-side_menu.title-item>
        <x-side_menu.title-item>Termination </x-side_menu.title-item>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            Administration
            </a>
        </div>
        
        <x-side_menu.title-item>Assets </x-side_menu.title-item>
        <x-side_menu.dropdown name="Jobs " >
            <x-side_menu.item>User Dasboard </x-side_menu.item>
            <x-side_menu.item>Jobs Dasboard </x-side_menu.item>
            <x-side_menu.item>Manage Jobs </x-side_menu.item>
            <x-side_menu.item>Manage Resumes </x-side_menu.item>
            <x-side_menu.item>Shortlist Candidates </x-side_menu.item>
            <x-side_menu.item>Interview Questions </x-side_menu.item>
            <x-side_menu.item>Offer Approvals </x-side_menu.item>
            <x-side_menu.item>Experience Level </x-side_menu.item>
            <x-side_menu.item>Candidates List </x-side_menu.item>
            <x-side_menu.item>Schedule timing </x-side_menu.item>
            <x-side_menu.item>Aptitude Results </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.title-item>Know;edgebase </x-side_menu.title-item>
        <x-side_menu.title-item>Activities </x-side_menu.title-item>
        <x-side_menu.title-item>Users </x-side_menu.title-item>
        <x-side_menu.title-item>Settings </x-side_menu.title-item>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            Pages
            </a>
        </div>

        <x-side_menu.dropdown name="Profile " >
            <x-side_menu.item>Employee Profile</x-side_menu.item>
            <x-side_menu.item>Client Profile </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.dropdown name="Authentication " >
            <x-side_menu.item>Login</x-side_menu.item>
            <x-side_menu.item>Register </x-side_menu.item>
            <x-side_menu.item>Forgot Password </x-side_menu.item>
            <x-side_menu.item>OTP </x-side_menu.item>
            <x-side_menu.item>Lock Screen </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.dropdown name="Error Pages " >
            <x-side_menu.item>404 Error</x-side_menu.item>
            <x-side_menu.item>500 Error </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.dropdown name="Subscriptions " >
            <x-side_menu.item>Subscriptions (Admin) </x-side_menu.item>
            <x-side_menu.item>Subscriptions (Company) </x-side_menu.item>
            <x-side_menu.item>Subscribed Companies </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.dropdown name="Pages " >
            <x-side_menu.item>Search </x-side_menu.item>
            <x-side_menu.item>FAQ </x-side_menu.item>
            <x-side_menu.item>Terms </x-side_menu.item>
            <x-side_menu.item>Privacy Policy </x-side_menu.item>
            <x-side_menu.item>Blank Page </x-side_menu.item>
        </x-side_menu.dropdown>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            UI Interface
            </a>
        </div>

        <x-side_menu.title-item>Components </x-side_menu.title-item>
        <x-side_menu.dropdown name="Forms " >
            <x-side_menu.item>Basic Input </x-side_menu.item>
            <x-side_menu.item>Input Groups </x-side_menu.item>
            <x-side_menu.item>Horizontal Form </x-side_menu.item>
            <x-side_menu.item>Vertical Form  </x-side_menu.item>
            <x-side_menu.item>Form Mask </x-side_menu.item>
            <x-side_menu.item>Form Validation </x-side_menu.item>
        </x-side_menu.dropdown>
        <x-side_menu.dropdown name="Tables " >
            <x-side_menu.item>Basic Tables </x-side_menu.item>
            <x-side_menu.item>Data Table </x-side_menu.item>
        </x-side_menu.dropdown>

        <div class="flex flex-shrink-0 px-4 py-4 ">
            <a href="#" class=" text-sm font-bold tracking-widest text-gray-300 
                                rounded-lg dark-mode:text-white focus:outline-none 
                                focus:shadow-outline">
            Extra
            </a>
        </div>
        
        <x-side_menu.title-item>Documentation </x-side_menu.title-item>
        <x-side_menu.title-item>Change Log </x-side_menu.title-item>
        <x-side_menu.title-item>Components </x-side_menu.title-item>
        <x-side_menu.dropdown name="Multi Level" >
            <x-side_menu.item>Level 1</x-side_menu.item>
            <x-side_menu.item>Level 2</x-side_menu.item>
        </x-side_menu.dropdown>
        
    </nav>

  </div>
</div>


<div class="flex justify-center">
    <div x-cloak x-data="{ dropdownOpen: false }" class="relative">

        <button @click="dropdownOpen = !dropdownOpen" 
                class=" relative z-10  rounded-md 
                        focus:outline-none">
                        
            <div class="flex flex-row">
                <img src="/images/united-kingdom.png" alt="Cannot open !" 
                    class=" object-left mr-1" width="25px" height="20px">
                <h1>English</h1>
                <svg class="h-5 w-5 text-gray-800 mt-1 ml-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                </svg>
            </div>
        </button>

        <div x-show="dropdownOpen" @click="dropdownOpen = false" 
                class="fixed inset-0
                        h-full w-full z-10"></div>

        <div x-show="dropdownOpen" 
            class="absolute -inset-x-8  py-2 w-30 
                    bg-white rounded-sm shadow-xl z-20
                    "
            style="top:4rem;">

            <div class="flex flex-row self-center px-2 hover:bg-gray-200">
                <img src="/images/united-kingdom.png" alt="Cannot open !" 
                    class=" object-left mr-3 " 
                    width="25px" height="10px">

                <a href="#" 
                    class="block p-2 text-xs capitalize 
                            text-gray-700  
                            ">
                English
                </a>
            </div>

            <div class="flex flex-row self-center px-2 hover:bg-gray-200">
                <img src="/images/france.png" alt="Cannot open !" 
                    class=" object-left mr-3 " 
                    width="25px" height="10px">

                <a href="#" 
                    class="block p-2 text-xs capitalize 
                            text-gray-700  
                            ">
                French
                </a>
            </div>

            <div class="flex flex-row self-center px-2 hover:bg-gray-200">
                <img src="/images/spain.png" alt="Cannot open !" 
                    class=" object-left mr-3 " 
                    width="25px" height="10px">

                <a href="#" 
                    class="block p-2 text-xs capitalize 
                            text-gray-700  
                            ">
                Spanish
                </a>
            </div>

            <div class="flex flex-row self-center px-2 hover:bg-gray-200">
                <img src="/images/germany.png" alt="Cannot open !" 
                    class=" object-left mr-3 " 
                    width="25px" height="10px">

                <a href="#" 
                    class="block p-2 text-xs capitalize 
                            text-gray-700  
                            ">
                German
                </a>
            </div>
            
        </div>
    </div>
</div>
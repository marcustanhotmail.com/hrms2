
<nav class=" grid grid-cols-12 
            bg-gradient-to-r from-yellow-400 via-red-500 to-pink-500 fixed w-full z-30">

    <a href="/">
        <div class=" col-span-1 pl-16 pt-4 text-center cursor-pointer">
        
        <!-- <span class="text-white"><a href="/">Logo</a></span> -->
            <span class=" bg-white ">
            
                <svg class="h-10 w-10 " version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">
                <circle style="fill:#ECF0F1;stroke:#38454F;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" cx="30" cy="30" r="28"/>
                <path style="fill:#556080;stroke:#38454F;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" d="M30,51
                    C18.42,51,9,41.579,9,30S18.42,9,30,9s21,9.421,21,21S41.58,51,30,51z"/>
                <path style="fill:#E64C3C;stroke:#38454F;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" d="M30,44
                    c-7.72,0-14-6.28-14-14s6.28-14,14-14s14,6.28,14,14S37.72,44,30,44z"/>
                <path style="fill:#F3D55B;stroke:#38454F;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" d="M30,37
                    c-3.859,0-7-3.141-7-7s3.141-7,7-7s7,3.141,7,7S33.859,37,30,37z"/>
                <path style="fill:#ECF0F1;" d="M30,60c-0.553,0-1-0.448-1-1V1c0-0.552,0.447-1,1-1s1,0.448,1,1v58C31,59.552,30.553,60,30,60z"/>
                <path style="fill:#ECF0F1;" d="M59,31H1c-0.553,0-1-0.448-1-1s0.447-1,1-1h58c0.553,0,1,0.448,1,1S59.553,31,59,31z"/>
            
                </svg>

            </span>
            
        </div>
    </a>

<div class=" col-span-5 p-4  w-full ">
    <h1 class="text-center pt-1 text-white text-xl font-mono font-semibold">Dreamguy`s Technologies</h1>
</div>

<div class=" col-span-3 p-4  ">

    <div class="flex justify-end opacity-40">
        
        <input type="text" placeholder="search here" 
            class=" flex-none rounded-l-3xl outline-none p-2 pl-5  " style="width: 200px;">

        <div class="bg-white rounded-r-3xl">
            <svg xmlns="http://www.w3.org/2000/svg" 
                class=" flex-none h-5 w-5 self-center mt-3 mr-3  "  fill="none" viewBox="0 0 24 24" stroke="gray">

            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg>
        </div>
        
    </div>
    
</div>

<div class=" col-span-1 p-4 hover:bg-yellow-400 justify-center pt-5">
    
    <x-header_menu.language-dropdown ></x-header_menu.language-dropdown>
    
</div>

<div class="col-span-1 " >

    <div class="flex flex-row ">

        <div class=" flex-grow pt-5 pr-3 pl-5 pb-7 hover:bg-yellow-400 cursor-pointer">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
            </svg>
        </div>

        <div class=" flex-grow pt-5 pr-3 pl-5 pb-7 hover:bg-yellow-400 cursor-pointer">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 " fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
        </div>
        
    </div>

</div>


<div class=" col-span-1 p-4 hover:bg-yellow-400 cursor-pointer">
    <div class="flex flex-row justify-between">
        <img src="/images/1.png" 
            class="rounded-full" 
            alt="" width="30px" height="30px">

        <span class="text-center mt-1 ml-1">Admin</span>

        <svg class="h-5 w-5 text-gray-800 mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
        </svg>
    </div>
</div>

</nav>
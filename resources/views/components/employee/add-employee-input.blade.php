@props(['type' => 'text' , 'require' => '', 'id']) 

<td class=" pr-7 pb-6">
    <div class="flex flex-col gap-x-1 gap-y-2 font-semibold w-auto">
        <div>
            <label for="{{ $id }}">{{ $slot }}</label>
        </div>
        <input  id="{{ $id }}" 
                name="{{ $id }}"
                class="p-2 border border-gray-300 rounded focus:outline-none " 
                type="{{ $type }}" 
                {{ ($require == '1') ? 'required' : '' }}
                value="{{ old($id) }}">

        

        @error($id)

            <div class=" relative ">
                <span class=" absolute text-red-500 text-xs mt-2 -top-3" > {{ $message }} </span>
            </div>

        @else

            <div class=" relative ">
                <span class=" absolute text-red-500 text-xs mt-2 -top-3" >  </span>
            </div>

        @enderror    

    </div>
</td>
@props(['id'])

<td class="py-2 pl-6" >
    <input type="checkbox" class=" " id="{{$id}}" name="{{ $id }}" {{ old($id) == 'on' ? 'checked' : ''}}>
</td>
@props(['value', 'oldValue' => ''])

<option 
        value="{{$value}}" 
        class=" h-14 text-lg hover:bg-yellow-700 "
        {{ $oldValue == $value ? 'selected' : '' }}
        >
    {{ $slot }}
 </option>
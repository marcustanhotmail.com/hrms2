<!-- This example requires Tailwind CSS v2.0+ -->
<div class="fixed inset-0 overflow-y-auto z-40" aria-labelledby="modal-title" role="dialog" aria-modal="true">
                        
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

        <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true" @click="open = false" ></div>

        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true" >&#8203;</span>

        <!-- sm there will controlling the sizing of the pop out modele view, because our screen size is sm  -->
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all 
                    sm:my-8 sm:align-middle sm:w-6/12"
            >

            <form method="POST" action="/add_employee" >
                @csrf

                <div class="flex flex-col bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">

                    <div>
                        <h1 class=" text-center font-bold text-xl mt-4 mb-12 ">Add Employee</h1>
                    </div>

                    <div class=" w-auto h-auto">

                        <table class=" min-w-full table-fixed">
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            
                            <tr>
                                
                                <x-employee.add-employee-input type="text" id="first_name"> 
                                    First Name 
                                </x-employee.add-employee-input>

                                <x-employee.add-employee-input type="text" id="last_name">
                                Last Name
                                </x-employee.add-employee-input>
                            </tr>

                            <tr>
                                <x-employee.add-employee-input type="text" id="username">
                                    Username
                                </x-employee.add-employee-input>

                                <x-employee.add-employee-input type="text" id="email">
                                    Email
                                </x-employee.add-employee-input>
                            </tr>

                            <tr>
                                <x-employee.add-employee-input type="password" id="password">
                                    Password
                                </x-employee.add-employee-input>

                                <x-employee.add-employee-input type="password" id="confirmPassword">
                                    Confirm Password
                                </x-employee.add-employee-input>
                            </tr>

                            <tr>
                                <x-employee.add-employee-input type="text" id="employee_id">
                                    Employee ID
                                </x-employee.add-employee-input>


                                <x-employee.add-employee-input type="date" id="join_date">
                                    Joing Date
                                </x-employee.add-employee-input>
                            </tr>

                            <tr>
                                <x-employee.add-employee-input type="text" id="phone">
                                    Phone
                                </x-employee.add-employee-input>

                                <x-employee.add-employee-dropdown id="company_id" name="Company"
                                    :items="[
                                        '1' => 'Global Technologies',
                                        '2' => 'Delta Infotech'
                                    ]"
                                    >
                                </x-employee.add-employee-dropdown>
                            </tr>

                            <tr>
                                <x-employee.add-employee-dropdown id="department_id" name="Department"
                                    :items="[
                                        '1' => 'Web Development',
                                        '2' => 'IT Management',
                                        '3' => 'Marketing'
                                    ]"
                                    >
                                </x-employee.add-employee-dropdown>

                                <x-employee.add-employee-dropdown id="designation_id" name="Designation"
                                    :items="[
                                        '1' => 'Web Designer',
                                        '2' => 'Web Developer',
                                        '3' => 'Android Developer'
                                    ]"
                                    >
                                </x-employee.add-employee-dropdown>
                            </tr>
                        </table>

                    </div>

                    <div class=" mt-6">

                        <table class="w-full">
                            <thead class=" font-semibold border-t border-b border-gray-400 ">
                                <tr  >
                                    <th class="p-2" >Module Permission</th>
                                    <th class="p-2" >Read</th>
                                    <th class="p-2" >Write</th>
                                    <th class="p-2" >Create</th>
                                    <th class="p-2" >Delete</th>
                                    <th class="p-2" >Import</th>
                                    <th class="p-2" >Export</th>
                                </tr>
                            </thead>

                            <tbody class=" w-full">
                                <tr class=" bg-gray-300 border-t border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Holiday</td>
                                    <x-employee.add-employee-checkbox id="holiday_read" />
                                    <x-employee.add-employee-checkbox id="holiday_write" />
                                    <x-employee.add-employee-checkbox id="holiday_create" />
                                    <x-employee.add-employee-checkbox id="holiday_delete" />
                                    <x-employee.add-employee-checkbox id="holiday_import" />
                                    <x-employee.add-employee-checkbox id="holiday_export" />
                                </tr>

                                <tr class="border-t  border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Leaves</td>
                                    <x-employee.add-employee-checkbox id="leave_read" />
                                    <x-employee.add-employee-checkbox id="leave_write" />
                                    <x-employee.add-employee-checkbox id="leave_create" />
                                    <x-employee.add-employee-checkbox id="leave_delete" />
                                    <x-employee.add-employee-checkbox id="leave_import" />
                                    <x-employee.add-employee-checkbox id="leave_export" />
                                </tr>

                                <tr class="border-t bg-gray-300 border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Clients</td>
                                    <x-employee.add-employee-checkbox id="client_read" />
                                    <x-employee.add-employee-checkbox id="client_write" />
                                    <x-employee.add-employee-checkbox id="client_create" />
                                    <x-employee.add-employee-checkbox id="client_delete" />
                                    <x-employee.add-employee-checkbox id="client_import" />
                                    <x-employee.add-employee-checkbox id="client_export" />
                                </tr>

                                <tr class="border-t  border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Project</td>
                                    <x-employee.add-employee-checkbox id="project_read" />
                                    <x-employee.add-employee-checkbox id="project_write" />
                                    <x-employee.add-employee-checkbox id="project_create" />
                                    <x-employee.add-employee-checkbox id="project_delete" />
                                    <x-employee.add-employee-checkbox id="project_import" />
                                    <x-employee.add-employee-checkbox id="project_export" />
                                </tr>

                                <tr class="border-t bg-gray-300 border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Tasks</td>
                                    <x-employee.add-employee-checkbox id="task_read" />
                                    <x-employee.add-employee-checkbox id="task_write" />
                                    <x-employee.add-employee-checkbox id="task_create" />
                                    <x-employee.add-employee-checkbox id="task_delete" />
                                    <x-employee.add-employee-checkbox id="task_import" />
                                    <x-employee.add-employee-checkbox id="task_export" />
                                </tr>

                                <tr class="border-t  border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Chats</td>
                                    <x-employee.add-employee-checkbox id="chat_read" />
                                    <x-employee.add-employee-checkbox id="chat_write" />
                                    <x-employee.add-employee-checkbox id="chat_create" />
                                    <x-employee.add-employee-checkbox id="chat_delete" />
                                    <x-employee.add-employee-checkbox id="chat_import" />
                                    <x-employee.add-employee-checkbox id="chat_export" />
                                </tr>

                                <tr class="border-t bg-gray-300 border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Assets</td>
                                    <x-employee.add-employee-checkbox id="assets_read" />
                                    <x-employee.add-employee-checkbox id="assets_write" />
                                    <x-employee.add-employee-checkbox id="assets_create" />
                                    <x-employee.add-employee-checkbox id="assets_delete" />
                                    <x-employee.add-employee-checkbox id="assets_import" />
                                    <x-employee.add-employee-checkbox id="assets_export" />
                                </tr>

                                <tr class="border-t  border-b border-gray-400" style=" height:50px;">
                                    <td class=" p-2" >Timing Sheets</td>
                                    <x-employee.add-employee-checkbox id="t_sheet_read" />
                                    <x-employee.add-employee-checkbox id="t_sheet_write" />
                                    <x-employee.add-employee-checkbox id="t_sheet_create" />
                                    <x-employee.add-employee-checkbox id="t_sheet_delete" />
                                    <x-employee.add-employee-checkbox id="t_sheet_import" />
                                    <x-employee.add-employee-checkbox id="t_sheet_export" />
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    
                </div>

                <div class="bg-gray-50 px-4 py-3 mb-5
                              sm:px-6 sm:flex sm:flex-row justify-center">

                    <button type="submit" 
                            class="w-full rounded-3xl border border-transparent shadow-sm px-14 py-2 
                            bg-yellow-400 font-bold text-white 
                            hover:bg-yellow-500
                            focus:outline-none
                            sm:ml-3 sm:w-auto sm:text-lg" >
                        Submit
                    </button>
                    
                </div>
            
            </form>

        </div>
    </div>
</div>
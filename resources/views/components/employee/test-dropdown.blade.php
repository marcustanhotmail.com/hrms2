
<div class="flex flex-col gap-x-1 gap-y-2 font-semibold">
	<div>
		<label for="department_id">department_id</label>
	</div>

	<select id="department_id" 
			name="department_id"
			class=" px-2 py-2 border border-gray-300 rounded outline-none h-auto">

		<option value="select department_id" class="text-lg p-2  " disabled selected> 
			Select department_id
		</option>

		
		@foreach ([
					'1' => 'Web Development',
					'2' => 'IT Management',
					'3' => 'Marketing'
				] as $item)
		
			<x-employee.add-employee-dropdown-item item="{{$item}}">
				{{ $item }}
			</x-employee.add-employee-dropdown-item>

		@endforeach
		
	</select>

	@error('department_id')
		<p class="text-red-500 text-xs mt-2"   > {{ $message }} </p>
	@enderror

</div>
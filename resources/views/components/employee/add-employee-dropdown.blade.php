@props(['id', 'items', 'name', 'require' => '1'])

<td class=" pr-7 pb-6">
    <div class="flex flex-col gap-x-1 gap-y-2 font-semibold">
        <div>
            <label for="{{$id}}">{{ ucwords($name) }}</label>
        </div>

        <select id="{{$id}}" 
                name="{{ $id }}"
                class=" px-2 py-2 border border-gray-300 rounded outline-none h-auto"
                {{ ($require == '1') ? 'required' : '' }}
                >

            <option value="select {{ $id }}" class="text-lg p-2  " disabled selected> 
                Select {{ ucwords($name) }}
            </option>

            @foreach ($items as $item)
            
                <x-employee.add-employee-dropdown-item value="{{ $loop->iteration }}" oldValue="{{ old($id) }}">
                    {{ $item }}
                </x-employee.add-employee-dropdown-item>

            @endforeach
            
        </select>

        <div>
            
            @error($id)
                <div class=" relative ">

                    <span class="absolute text-red-500 text-xs mt-2 -top-3"   > {{ $message }} </sapn>
                    
                </div>

            @else

                <div class=" relative ">
                    <span class=" absolute text-red-500 text-xs mt-2 -top-3" >  </span>
                </div>
                
            @enderror

        </div>

    </div>
</td>
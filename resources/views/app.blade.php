<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    
    <script src="//unpkg.com/alpinejs" defer></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    <style>
    /* Untuk Scrollbar in side navigation */
    /* width */
    .mynav::-webkit-scrollbar {
    width: 8px;
    }

    /* Track */
    .mynav::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey; 
    border-radius: 10px;
    }
    
    /* Handle */
    .mynav::-webkit-scrollbar-thumb {
    background: gray; 
    border-radius: 10px;
    }

    /* Handle on hover */
    .mynav::-webkit-scrollbar-thumb:hover {
    background: darkgrey; 
    }

    /* Untuk input field transition */
    .input:focus+.label,
    .input:active+.label,
    .input.filled+.label {
        font-size: .75rem;
        transition: all 0.2s ease-out;
        top: -0.1rem;
    }

    .label {
        transition: all 0.2s ease-out;
        top: 0.4rem;
      	left: 0;
    }
    </style>
    
    <title>Document</title>

</head>
<body class=" m-0 bg-gray-200">
    
    <header>
        @include('./components/header_menu/c_hearder_menu')
    </header>

    <main>

        <div class=" flex flex-row"  >

            <div class="mynav bg-gray-700 fixed overflow-auto overflow-x-hidden" style=" margin-top: 72px; max-height:91%;">
                @include('./components/side_menu/c_side_menu')
            </div>

            <div class=" h-screen  flex-grow" style=" margin-top: 72px; margin-left:264px; ">
                @include('./components/employee/all-employee')
            </div>

        </div>

    </main>
    

</body>
</html>
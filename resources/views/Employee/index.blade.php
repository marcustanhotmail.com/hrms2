<x-layout>

<div class="mt-10 ml-10 mr-20 " >
    
    <div class="ml-1 mb-12 flex flex-row gap-2 w-full">
        <h1 class="text-letf text-xl w-3/4 font-bold">All employees</h1>  

        <button class=" p-2 bg-white rounded" >
            <svg version="1.1" id="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                width="23px" height="23px" viewBox="0 0 32 32" style="enable-background:new 0 0 32 32;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:none;}
            </style>
            <rect class="st0" width="32" height="32"/>
            <rect x="14" y="4" width="4" height="4"/>
            <rect x="4" y="4" width="4" height="4"/>
            <rect x="24" y="4" width="4" height="4"/>
            <rect x="14" y="14" width="4" height="4"/>
            <rect x="4" y="14" width="4" height="4"/>
            <rect x="24" y="14" width="4" height="4"/>
            <rect x="14" y="24" width="4" height="4"/>
            <rect x="4" y="24" width="4" height="4"/>
            <rect x="24" y="24" width="4" height="4"/>
            </svg>
        </button>

        <button class=" p-2 bg-white rounded" >
            <svg width="23px" height="23px" viewBox="0 -4.17 29.82 29.82" id="_10_-_Recent_App" data-name="10 - Recent App" xmlns="http://www.w3.org/2000/svg" fill=" gray ">
                <path id="Path_170" data-name="Path 170" d="M30.91,7.392v0A2.39,2.39,0,0,0,28.52,5H3.48A2.39,2.39,0,0,0,1.09,7.39v0a2.39,2.39,0,0,0,2.39,2.39H28.52A2.39,2.39,0,0,0,30.91,7.392Zm-2,0v0a.391.391,0,0,1-.39.39H3.48a.391.391,0,0,1-.39-.39v0A.391.391,0,0,1,3.48,7H28.52A.391.391,0,0,1,28.91,7.39Z" transform="translate(-1.09 -5)" fill-rule="evenodd"/>
                <path id="Path_171" data-name="Path 171" d="M30.91,15.738v0a2.39,2.39,0,0,0-2.39-2.39H3.48a2.39,2.39,0,0,0-2.39,2.39v0a2.39,2.39,0,0,0,2.39,2.39H28.52A2.39,2.39,0,0,0,30.91,15.738Zm-2,0v0a.391.391,0,0,1-.39.39H3.48a.391.391,0,0,1-.39-.39v0a.391.391,0,0,1,.39-.39H28.52A.391.391,0,0,1,28.91,15.736Z" transform="translate(-1.09 -5)" fill-rule="evenodd"/>
                <path id="Path_172" data-name="Path 172" d="M30.91,24.084v0a2.39,2.39,0,0,0-2.39-2.39H3.48a2.39,2.39,0,0,0-2.39,2.39v0a2.39,2.39,0,0,0,2.39,2.39H28.52A2.39,2.39,0,0,0,30.91,24.084Zm-2,0v0a.391.391,0,0,1-.39.39H3.48a.391.391,0,0,1-.39-.39v0a.391.391,0,0,1,.39-.39H28.52A.391.391,0,0,1,28.91,24.082Z" transform="translate(-1.09 -5)" fill-rule="evenodd"/>
            </svg>
        </button>

        
        <div x-cloak x-data="{ open: false }" >
            <button class=" p-2  rounded-3xl flex-1 bg-yellow-400 
                            flex flex-row justify-start 
                        hover:bg-yellow-500 "
                    @click="open = !open">

                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 60.364 60.364" style="enable-background:new 0 0 60.364 60.364;" xml:space="preserve" fill=" white " 
                    width="18px" height="18px" class=" pl-2 pt-1">
                    <g>
                        <path d="M54.454,23.18l-18.609-0.002L35.844,5.91C35.845,2.646,33.198,0,29.934,0c-3.263,0-5.909,2.646-5.909,5.91v17.269
                            L5.91,23.178C2.646,23.179,0,25.825,0,29.088c0.002,3.264,2.646,5.909,5.91,5.909h18.115v19.457c0,3.267,2.646,5.91,5.91,5.91
                            c3.264,0,5.909-2.646,5.91-5.908V34.997h18.611c3.262,0,5.908-2.645,5.908-5.907C60.367,25.824,57.718,23.178,54.454,23.18z"/>
                    </g>
                </svg>

                <p class=" text-white text-center pl-4 pr-3">Add Employee</p>
            </button>

            
            @if($errors->any()) 
                <div x-data="{ open: true }" >
                    <div x-show="open" >
                        <x-employee.add-employee-model />    
                    </div>
                </div>
            @endif

            <div 
                x-show="open" 
                >

                <x-employee.add-employee-model />    
            </div>
            

        </div>
        
    </div>

    <div class="flex flex-row gap-6 my-5 w-full z-0">

        <div class=" relative min-h-full flex-1">
            <input 
                class="input border border-gray-400 appearance-none rounded  pr-16 pl-3 py-3 pt-5 pb-2 focus 
                      focus:outline-none h-full"
                id="search_employee_id" type="text" >

            <label for="search_employee_id" 
                    class="label absolute mb-0 -mt-2 pt-4 pl-3 leading-tighter text-gray-400 text-base mt-2 cursor-text">
                    Employee ID</label>

        </div>

        <div class=" relative min-h-full flex-1">
            <input 
                class="input border border-gray-400 appearance-none rounded  pr-16 pl-3 pt-5 pb-2 focus 
                      focus:outline-none h-full"
                id="search_employee_name" type="text" >

            <label for="search_employee_name" 
                    class="label absolute mb-0 -mt-2 pt-4 pl-3 leading-tighter text-gray-400 text-base mt-2 cursor-text">
                    Employee Name</label>

        </div>

        <!-- Drop down list -->
        <div class=" bg-white cursor-pointer border border-gray-400 rounded w-full flex-1" >

            <div x-cloak x-data="{ dropdownOpen: false }" class="relative ">

                <button @click="dropdownOpen = !dropdownOpen" 
                        class=" relative z-10  rounded-md 
                                focus:outline-none">
                                
                    <div class="flex flex-col text-left">
                        <p class=" text-xs text-gray-500 px-2 ">Designation</p>
                        <p class=" text-base px-2 pb-2">Select Designation</p>

                        <div class="absolute left-56 top-5">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 386.257 386.257" style="enable-background:new 0 0 386.257 386.257;" xml:space="preserve" fill=" gray "
                                class="w-3 h-3 ">
                            <polygon points="0,96.879 193.129,289.379 386.257,96.879 "/>
                            </svg>
                        </div>
                    
                    </div>
                </button>

                <div x-show="dropdownOpen" @click="dropdownOpen = false" 
                        class="fixed inset-0
                                h-full w-full z-10"></div>

                <div x-show="dropdownOpen" 
                    class="absolute inset-x-0 top-14  py-2
                            bg-white rounded-sm shadow-xl z-20
                            "
                    style="width:auto">

                    <div class="flex flex-col self-center w-full">

                        <a href="#" class="block p-2 text-xs capitalize text-gray-700 hover:bg-yellow-400  ">
                            Select Designation
                        </a>

                        <a href="#" class="block p-2 text-xs capitalize text-gray-700 hover:bg-yellow-400  ">
                            Web Developer
                        </a>

                        <a href="#" class="block p-2 text-xs capitalize text-gray-700 hover:bg-yellow-400  ">
                            Web Designer
                        </a>

                        <a href="#" class="block p-2 text-xs capitalize text-gray-700 hover:bg-yellow-400  ">
                            Android Developer
                        </a>

                        <a href="#" class="block p-2 text-xs capitalize text-gray-700 hover:bg-yellow-400  ">
                            Ios Developer
                        </a>
                    </div>
                    
                </div>

            </div>

        </div>

        <button class=" uppercase bg-green-400 px-5 rounded-md min-h-full outline-none text-white flex-1">Search</button>

    </div>
</div>

<div class="m-10 flex flex-row flex-wrap gap-10 pb-10" >

    @foreach ($employee as $e)
        <div class="flex flex-col bg-white rounded-md  justify-center p-2 h-auto shadow-md" style="width:350px; min-height:200px;">
            
            <div class="w-full flex justify-center">
                <img src="/images/1.png" 
                            class="rounded-full " 
                            alt="" width="130px" height="130px">
            </div>
        
            <a href="/employee/{{ $e->id }}" ><p class=" text-center font-semibold">{{ $e->first_name }}</p></a>
            <a class=" text-center text-gray-500 text-sm mb-1 ">{{ ucwords($e->position) }} </a>
                
        </div>
    @endforeach

</div>

</x-layout>
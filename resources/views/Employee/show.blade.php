<x-layout>
    <!-- <h1>My name is {{ $employee->name }}</h1> -->
    <div class="flex flex-row flex-wrap gap-3 p-10 ">
        <div>
            <h1 class=" text-left text-3xl mb-3 ml-3"><strong>Profile</strong></h1>
        </div>

        <div class="p-2 h-auto min-w-full">

            <div class="grid grid-cols-5 pt-4 pb-6
                        border border-gray-400 
                        rounded-md bg-white divide-x-2 divide-gray-400 divide-dashed shadow-md">

                <div class=" col-span-2 
                    block min-h-full min-w-full">

                    <div class="grid grid-cols-5 gap-2 w-full h-full">

                        <div class=" col-span-2 w-full h-full flex justify-center">
                            <img src="/images/1.png" class="rounded-full self-start m-3" alt="" width="130px" height="130px">
                        </div>

                        <div class=" col-span-3 ">
                            <h1 class=" text-xl "><strong> {{ $employee->first_name }} </strong></h1>
                            <h4 class=" text-xs text-gray-400" >UI/UX Design Team</h4>

                            <h5 class=" text-xs text-gray-400 mt-2 mb-2" >{{ $employee->position }}</h5>

                            <h2 class=" ">Employee ID : <strong> {{ $employee->employee_id }} </strong> </h2>
                            <h5 class=" text-xs text-gray-400">Date of Join : {{ $employee->created_at }}</h5>

                            <button class=" rounded-lg text-white bg-gradient-to-r from-yellow-400 via-red-500 to-pink-500 
                                            px-4 py-2 mt-5 mb-5 ">
                                Send Message
                            </button>
                        </div>

                    </div>
                </div>

                <div class=" col-span-3 block w-full h-full pt-3 pb-3 pl-7 ">

                    <div class="grid grid-cols-5 justify-start ">
                        <div class=" col-span-1">
                            <h3 class="mb-1" >Phone:</h3>
                            <h3 class="mb-1" >Email:</h3>
                            <h3 class="mb-1" >Birthday:</h3>
                            <h3 class="mb-1" >Address:</h3>
                            <h3 class="mb-1" >Gender:</h3>
                            <h3 class="mb-1" >Reports to : </h3>
                        </div>

                        <div class=" col-span-4">
                            <h3 class="mb-1" >{{ $employee->phone }}</h3>
                            <h3 class="mb-1" ><a href="#" class=" text-blue-600">{{ $employee->email }}</a></h3>
                            <h3 class="mb-1" >{{ $employee->birthday }}</h3>
                            <h3 class="mb-1" >{{ $employee->address }}</h3>
                            <h3 class="mb-1" >{{ $employee->gender }}</h3>
                            <h3 class="mb-1" >{{ $employee->supervisor_id }}</h3>
                        </div>
                    </div>
                </div>

            </div>

            <div class=" flex flex-row justify-start w-full 
                        border border-gray-400 rounded-md bg-white shadow-md">

                <button    
                    class="p-3 text-gray-500 outline-none
                            border-b-4 border-transparent hover:border-yellow-400 focus:border-yellow-400 
                            hover:text-gray-900 hover:bg-gray-300 " 
                            autofocus> 
                    Profile </button>

                <button class="p-3 text-gray-500 outline-none
                               border-b-4 border-transparent hover:border-yellow-400 focus:border-yellow-400 
                             hover:text-gray-900 hover:bg-gray-300 focus:border-b-4" > Projects </button>
                             
                <button class="p-3 text-gray-500 outline-none
                               border-b-4 border-transparent hover:border-yellow-400 focus:border-yellow-400 
                             hover:text-gray-900 hover:bg-gray-300 focus:border-b-4" >  Bank & Statutory <span class=" text-sm text-red-500">(Admin Only)</span> </button>
            </div>

        </div>

        <div class="grid grid-cols-4 p-2 h-auto w-full gap-10 ">

            <div class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full ">

                <h1 class=" text-xl mb-3"><strong> Personal Information </strong></h1>

                <div class="grid grid-cols-2">
                    <div class=" col-span-1">
                        <h3 class=" mb-1 "> Passport No. </h3>
                        <h3 class=" mb-1 "> Passport Exp Date. </h3>
                        <h3 class=" mb-1 "> Tel </h3>
                        <h3 class=" mb-1 "> Nationality </h3>
                        <h3 class=" mb-1 "> Religion </h3>
                        <h3 class=" mb-1 "> Marital status </h3>
                        <h3 class=" mb-1 "> Employment of spouse </h3>
                        <h3 class=" mb-1 "> No. of children </h3>
                    </div>

                    <div class=" col-span-1">
                    
                        <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->passport) ? '-' : $employee->passport  }} </h3>
                        <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->passport_experied_at) ? '-' : $employee->passport_experied_at  }} </h3>
                        <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->tel) ? '-' : $employee->tel  }} </h3>
                        <h3 class=" text-gray-500 mb-1 "> - </h3>
                        <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->religion) ? '-' : $employee->religion  }} </h3>

                        
                        <h3 class=" text-gray-500 mb-1 "> {{ ( $employee->marital_status_id == '1' ) ? 'Single' : 'Married'  }} </h3>
                        <h3 class=" text-gray-500 mb-1 "> {{ ( $employee->employment_of_spouse == '0' ) ? 'No' : 'Yes'  }} </h3>
                        <h3 class=" text-gray-500 mb-1 "> {{ $employee->no_of_children }} </h3>
                    </div>
                </div>
            </div>

            <div class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full ">
                <h1 class=" text-xl mb-3"><strong> Emergency Contact </strong></h1>

                <div class="mb-2">
                    <table class=" w-full h-auto ">
                        <tr>
                            <td class=" w-2/5 ">Primary </td>
                            <td class=" w-3/5 "> </td>
                        </tr>
                        <tr>
                            <td>Name: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ $employee->primary_name }} </h3></td>
                        </tr>
                        <tr>
                            <td>Relationship: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ $employee->primary_relationship }} </h3></td>
                        </tr>
                        <tr >
                            <td>Phone: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ $employee->primary_phone }} </h3></td>
                        </tr>

                        <tr class="border-b border-gray-300  h-5">
                        </tr>
                        <tr class="  h-5">
                        </tr>

                        <tr>
                            <td>Secondary  </td>
                            <td>  </td>
                        </tr>
                        <tr>
                            <td>Name: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ empty($employee->secondary_name) ? '-' : $employee->secondary_name  }} </h3></td>
                        </tr>
                        <tr>
                            <td>Relationship: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ empty($employee->secondary_relationship) ? '-' : $employee->secondary_relationship  }} </h3></td>
                        </tr>
                        <tr>
                            <td>Phone: </td>
                            <td><h3 class=" text-gray-500 mb-1 "> {{ empty($employee->secondary_phone) ? '-' : $employee->secondary_phone  }} </h3></td>
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
        
        <div class="grid grid-cols-4 p-2 w-full h-auto gap-10">

            <div class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full ">

                <h1 class=" text-xl mb-3 " ><strong > Bank Information </strong></h1>

                    <div class="grid grid-cols-2">
                        <div class=" col-span-1">
                            <h3 class=" mb-1 "> Bank name </h3>
                            <h3 class=" mb-1 "> Bank account No. </h3>
                            <h3 class=" mb-1 "> IFSC Code </h3>
                            <h3 class=" mb-1 "> PAN No </h3>
                        </div>

                        <div class=" col-span-1">
                            <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->bank) ? '-' : $employee->bank  }} </h3>
                            <h3 class=" text-gray-500 mb-1 "> {{ empty($employee->account) ? '-' : $employee->account  }} </h3>
                            <h3 class=" text-gray-500 mb-1 "> - </h3>
                            <h3 class=" text-gray-500 mb-1 "> - </h3>
                        </div>
                    </div>
            </div>

            <div class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full ">

                <h1 class=" text-xl mb-3 ml-3"><strong> Family Information </strong></h1>

                    <div class="m-3 ">
                        <table class="table-auto  w-full ">
                            <thead class=" text-left text-sm">
                                <tr>
                                <th class="border-b border-t border-gray-400 px-2 py-2" >Name</th>
                                <th class="border-b border-t border-gray-400 px-2 py-2" >Relationship</th>
                                <th class="border-b border-t border-gray-400 px-2 py-2" >Date of Birth</th>
                                <th class="border-b border-t border-gray-400 px-2 py-2" >Phone</th>
                                </tr>
                            </thead>
                            <tbody class=" text-left text-sm font-semibold">
                                @foreach ($employee->families as $family)
                                    <tr>
                                    <!-- <td class="p-2" >{{ $family->name }}</td> -->
                                    <td class="p-2" >{{ strtok($family->name, " ") }}</td>
                                    <td class="p-2" >{{ ucwords($family->relationship) }}</td>
                                    <td class="p-2" >{{ $family->birthday }}</td>
                                    <td class="p-2" >{{ $family->phone }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
            </div>

        </div>

        <div class="grid grid-cols-4 p-2 w-full h-auto gap-10" >

            <div  class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full">

                <h1 class=" text-xl mb-3 ml-3"><strong> Education Information </strong></h1>

                <div class=" h-auto">

                    <div class="relative m-2 w-full">
                        <!-- Here is controlling the gray color line -->
                        <div class="border-r-2 border-gray-200 absolute h-full top-2" style="left: 15px"></div>
                            
                        <ul class="list-none m-0 p-0">

                            @foreach ($employee->educations as $education)
                            <li class="mb-2">

                                <div class="flex items-center mb-1">
                                    <!-- Here is controlling the gray color ball -->
                                    <div class="bg-gray-200 rounded-full h-3 w-3" style="margin-left: 10px;"></div>

                                    <div class="flex-1 ml-6 font-bold text-sm text-gray-600">
                                        <p> {{ $education->university_name }} </p>
                                    </div>
                                </div>

                                <div class="ml-12">
                                    <p class=" text-sm text-gray-500"> {{ $education->major }}  </p>
                                    <p class=" text-xs text-gray-400 mt-1"> {{ $education->duration_start }} - {{ $education->duration_end }}</p>
                                </div>

                            </li>
                            @endforeach

                        </ul>
                    </div>

                </div>
                
            </div>


            <div class=" col-span-2 block  bg-white p-4 shadow-md
                         border border-gray-400 rounded min-h-full min-w-full" >


                <h1 class=" text-xl mb-3 ml-3"><strong> Experience </strong></h1>

                    <div class="h-auto w-full">

                        <div class="relative  m-2 w-full">
                            <!-- Here is controlling the gray color line -->
                            <div class="border-r-2 border-gray-200 absolute h-full top-4" style="left: 15px"></div>
                                
                            <ul class="list-none m-0 p-0 ">

                                @foreach ($employee->experiences as $experience)
                                <li class="mb-2">

                                    <div class="flex items-center mb-1 ">
                                        <!-- Here is controlling the gray color ball -->
                                        <div class="bg-gray-200 rounded-full h-3 w-3" style="margin-left: 10px;"></div>

                                        <div class="flex-1 ml-6 font-bold text-sm text-gray-600 ">
                                            <p> {{ ucwords($experience->job_title) }} at {{ ucwords($experience->company_name) }} </p>
                                        </div>
                                    </div>

                                    <div class="ml-12">
                                        <p class=" text-xs text-gray-400 mt-1"> {{ $experience->start_date }} - {{ $experience->end_date }}</p>
                                    </div>

                                </li>
                                @endforeach

                            </ul>
                        </div>

                    </div>
                        
            </div>

            
        </div>

    </div>
</x-layout>
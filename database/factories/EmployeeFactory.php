<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $relationship = [
            'parent',
            'siblings',
            'spouse',
            'cousin'
        ];

        $job = [
            'Web Designer',
            'IT technician',
            'system analyst',
            'system admin',
            'Web Developer',
            'Android Developer',
            'Team Leader',
            'IOS Developer'
        ];

        return [
            //
            'first_name'=>$this->faker->name(),
            'phone'=>$this->faker->phoneNumber(),
            'email'=>$this->faker->safeEmail(),
            'birthday'=>$this->faker->date(),
            'employee_id' => $this->faker->numerify('FT-####'),
            
            'primary_name'=>$this->faker->name(),
            'primary_phone'=>$this->faker->phoneNumber(),
            'primary_relationship'=>$relationship[rand(0, 3)],

            //
            'position' => $job[rand(0, 7)],
        ];
    }
}

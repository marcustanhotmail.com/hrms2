<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFamilyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $relationship = [
            'parent',
            'siblings',
            'spouse',
            'cousin'
        ];

        return [
            //
            'relationship' => $relationship[rand(0, 3)],
            'name' => $this->faker->name(),
            'birthday' => $this->faker->date(),
            'phone' => $this->faker->phoneNumber()
        ];
    }
}

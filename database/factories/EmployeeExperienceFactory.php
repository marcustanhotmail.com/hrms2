<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeExperienceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $job = [
            'programmer',
            'IT technician',
            'system analyst',
            'system admin'
        ];
        
        return [
            //
            'company_name' =>$this->faker->company(),
            'job_title' =>$job[rand(0, 3)],
            'start_date' =>$this->faker->date(),
            'end_date' =>$this->faker->date(),
        ];
    }
}

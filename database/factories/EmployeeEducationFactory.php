<?php

namespace Database\Factories;

use Bezhanov\Faker\Provider\Educator;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeEducationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $faker = \Faker\Factory::create();
        //$faker->addProvider(new Educator($faker));
        $faker->addProvider(new \Bezhanov\Faker\Provider\Educator($faker));
        
        $duration_start = $faker->date();
        $duration_end = date('Y-m-d', strtotime("$duration_start + 3 years"));

        return [
            //
            'university_name'  => $faker->university,
            'major' => $faker->course,
            'duration_start' => $duration_start,
            'duration_end' => $duration_end,
        ];
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\Employee::factory(10)->hasfamilies(rand(2,3))->haseducations(rand(2,3))->hasexperiences(rand(1,3))->create();
    }
}

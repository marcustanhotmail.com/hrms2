<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('employees', function (Blueprint $table){
            $table->after('id', function($table) {
                $table->string('employee_id', 20)->nullable();
                $table->integer('company_id')->unsigned()->default(1);
                $table->integer('department_id')->unsigned()->default(1);
                $table->integer('designation_id')->unsigned()->default(1);
            });

            $table->after('position', function($table) {
                $table->date('join_date')->nullable()->useCurrent();
            });

            $table->string('last_name', 50)->after('first_name')->nullable();
            $table->string('username', 50)->after('last_name')->nullable();
            $table->text('password')->after('email')->nullable();

            $table->string('holiday_read')->nullable();
            $table->string('holiday_write')->nullable();
            $table->string('holiday_create')->nullable();
            $table->string('holiday_delete')->nullable();
            $table->string('holiday_import')->nullable();
            $table->string('holiday_export')->nullable();

            $table->string('leave_read')->nullable();
            $table->string('leave_write')->nullable();
            $table->string('leave_create')->nullable();
            $table->string('leave_delete')->nullable();
            $table->string('leave_import')->nullable();
            $table->string('leave_export')->nullable();

            $table->string('client_read')->nullable();
            $table->string('client_write')->nullable();
            $table->string('client_create')->nullable();
            $table->string('client_delete')->nullable();
            $table->string('client_import')->nullable();
            $table->string('client_export')->nullable();

            $table->string('project_read')->nullable();
            $table->string('project_write')->nullable();
            $table->string('project_create')->nullable();
            $table->string('project_delete')->nullable();
            $table->string('project_import')->nullable();
            $table->string('project_export')->nullable();

            $table->string('task_read')->nullable();
            $table->string('task_write')->nullable();
            $table->string('task_create')->nullable();
            $table->string('task_delete')->nullable();
            $table->string('task_import')->nullable();
            $table->string('task_export')->nullable();
            
            $table->string('chat_read')->nullable();
            $table->string('chat_write')->nullable();
            $table->string('chat_create')->nullable();
            $table->string('chat_delete')->nullable();
            $table->string('chat_import')->nullable();
            $table->string('chat_export')->nullable();

            $table->string('assets_read')->nullable();
            $table->string('assets_write')->nullable();
            $table->string('assets_create')->nullable();
            $table->string('assets_delete')->nullable();
            $table->string('assets_import')->nullable();
            $table->string('assets_export')->nullable();

            $table->string('t_sheet_read')->nullable();
            $table->string('t_sheet_write')->nullable();
            $table->string('t_sheet_create')->nullable();
            $table->string('t_sheet_delete')->nullable();
            $table->string('t_sheet_import')->nullable();
            $table->string('t_sheet_export')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('employees', function (Blueprint $table){
            $table->dropColumn(['employee_id', 'company_id', 'department_id', 
                        'join_date', 'first_name', 'last_name', 'username', 'password']);
        });
    }
}

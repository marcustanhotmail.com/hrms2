<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //employees
        Schema::create('employee_families', function (Blueprint $table) {
            $table->id();
            //$table->foreign('employee_id')->constraint('employees')->casadeOnDelete();
            $table->foreignId('employee_id')->constrained()->casadeOnDelete();
            $table->string('relationship',15);
            $table->string('name',100);
            $table->date('birthday')->nullable();
            $table->string('phone', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_families');
    }
}

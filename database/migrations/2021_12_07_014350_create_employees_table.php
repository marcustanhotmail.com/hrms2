<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('first_name',100)->default('');
            $table->string('phone',20)->nullable()->default('');
            $table->string('email',50)->nullable()->default('');
            $table->date('birthday')->nullable();
            $table->string('address',100)->nullable();
            $table->boolean('gender')->nullable();
            $table->bigInteger('supervisor_id')->nullable();

            $table->string('passport',100)->nullable();
            $table->date('passport_experied_at')->nullable();
            $table->string('tel',10)->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('religion')->unsigned()->nullable();
            $table->tinyinteger('marital_status_id')->unsigned()->nullable()->default(1);
            $table->boolean('employment_of_spouse', 100)->nullable()->default(false);
            $table->tinyinteger('no_of_children')->unsigned()->default(0);

            $table->string('bank',20)->nullable();
            $table->string('account', 20)->nullable();

            $table->string('primary_name', 100)->nullable()->default('');
            $table->string('primary_phone',20)->nullable()->default('');
            $table->string('primary_relationship',10)->nullable()->default('');

            $table->string('secondary_name', 100)->nullable();
            $table->string('secondary_phone',20)->nullable();
            $table->string('secondary_relationship',10)->nullable();

            //
            $table->string('position',100)->nullable()->default('');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

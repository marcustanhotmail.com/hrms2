<?php

use App\Http\Controllers\EmployeeController;
use App\Models\Employee;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Employee.index', [
        'employee' => Employee::all()
    ]);
});

Route::get('/employee/{employee:id}', [EmployeeController::class, 'show']);
Route::post('add_employee', [EmployeeController::class, 'store']);

Route::get('test', function () {
    return view('Testing.Test');
});

//Side Menu Design
Route::get('test1', function () {
    return view('Testing.sidebar');
});

Route::get('test2', function () {
    return view('Testing.sidebar2');
});

Route::get('test3', function () {
    return view('Testing.sidebar3');
});

Route::get('sidebar_final', function () {
    return view('Testing.sidebar_final');
});

<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    //
    public function index()
    {
        return view('Employee.index', [
            'employee' => Employee::all()
        ]);
    }

    public function show(Employee $employee)
    {
        return view('Employee.show', [
            'employee' => $employee
        ]);
    }

    public function create() 
    {
        //return view('Employee.create');
    }

    public function store() {
        //regex of validate the phone number
        // \+?(.*)(01)[0-9]{8,9}
        // \+?(6?)((01[12346789])(\d{7,8}))

        $request = request()->all();

        $rules = [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:50',
            'username' => 'required|max:50|unique:employees,username',
            'email' => 'required|email|max:50|unique:employees,email',
            'phone' => 'required|regex:/^\+?(6?)((01[12346789])(\d{7,8}))$/',
            'password' => 'required|min:7|max:7',
            'confirmPassword' => 'required|same:password|min:7|max:7',
            'join_date' => 'required|date',
            'employee_id' => 'required',
            'designation_id' => 'required',
            'company_id' => 'required',
            'department_id' => 'required',

            'holiday_read' => 'nullable',
            'holiday_write' => 'nullable',
            'holiday_create' => 'nullable',
            'holiday_delete' => 'nullable',
            'holiday_import' => 'nullable',
            'holiday_export' => 'nullable',

            'leave_read' => 'nullable',
            'leave_write ' => 'nullable',
            'leave_create' => 'nullable',
            'leave_delete' => 'nullable',
            'leave_import' => 'nullable',
            'leave_export' => 'nullable',

            'client_read' => 'nullable',
            'client_write' => 'nullable',
            'client_create' => 'nullable',
            'client_delete' => 'nullable',
            'client_import' => 'nullable',
            'client_export' => 'nullable',

            'project_read' => 'nullable',
            'project_write' => 'nullable',
            'project_create' => 'nullable',
            'project_delete' => 'nullable',
            'project_import' => 'nullable',
            'project_export' => 'nullable',

            'task_read' => 'nullable',
            'task_write' => 'nullable',
            'task_create' => 'nullable',
            'task_delete' => 'nullable',
            'task_import' => 'nullable',
            'task_export' => 'nullable',

            'chat_read' => 'nullable',
            'chat_write' => 'nullable',
            'chat_create' => 'nullable',
            'chat_delete' => 'nullable',
            'chat_import' => 'nullable',
            'chat_export' => 'nullable',

            'assets_read' => 'nullable',
            'assets_write' => 'nullable',
            'assets_create' => 'nullable',
            'assets_delete' => 'nullable',
            'assets_import' => 'nullable',
            'assets_export' => 'nullable',

            't_sheet_read' => 'nullable',
            't_sheet_write' => 'nullable',
            't_sheet_create' => 'nullable',
            't_sheet_delete' => 'nullable',
            't_sheet_import' => 'nullable',
            't_sheet_export' => 'nullable',
        ];

        $messages = [
            'required' => 'The :attribute field is required !',
            'email' => 'The approprate email format should be eg: kengloon1998@hotmail.com',
            'regex' => 'The approprate phone number format should be eg: +60127402104'
        ];

        $validator = Validator::make($request, $rules, $messages);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $datas = $validator->validated();
        $datas["password"] = Hash::make($datas["password"]);

        Employee::create($datas);


        return "SUCCESS";
        
    }
}
